const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express()
const PORT = process.env.PORT || 4000;

mongoose.connect('mongodb+srv://admin:admin123@cluster0.brekg.mongodb.net/BogsyVideoStoreapi?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, 'MongoDB connection Error'));
db.once('open', () => console.log('You are now connected to MongoDB'))

app.use(express.json());
app.use(cors());

const customerRoute = require('./routes/customerRoute')
const itemRoute = require('./routes/itemRoute')

app.use('/customer', customerRoute)
app.use('/item', itemRoute)


app.listen(PORT, () => console.log(`Server is running at port ${PORT}`))