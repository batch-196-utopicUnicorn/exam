const Item = require('../models/Item')

console.log('Connected to customer route')

module.exports.newItem = (req, res) =>{

	let newItem = new Item({
		title: req.body.title,
		price: req.body.price,
		type: req.body.type

	})

	newItem.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.viewAllItems = (req, res) => {

	Item.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.singleItem = (req, res) => {

	Item.findById(req.body.itemId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.editItem = (req, res) =>{

	//console.log()

	let updates = {
		title: req.body.title,
		price: req.body.price,
		type: req.body.type

	}

	Item.findByIdAndUpdate(req.params.itemId, updates, {new: true})
	.then(updated => res.send(updated))
	.catch(error => res.send(error))

}

module.exports.deleteItem = (req, res) => {

	Item.findByIdAndDelete(req.params.itemId, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/*module.exports.rent = async (req, res) => {

	

	let isUserUpdate = await Customer.findById(req.params.customerId)
	.then(user => {
		console.log(req.body.itemId)

		let rentItem = {
		itemId: req.body.itemId
	}

		user.rents.push(rentItem)
		return user.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
	})	
}*/