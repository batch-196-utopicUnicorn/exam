const Customer = require('../models/Customer')

console.log('Connected to customer route')

module.exports.newCustomer = (req, res) =>{

	let newUser = new Customer({
		fullName: req.body.fullName
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.viewAllCustomer = (req, res) => {

	Customer.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}



module.exports.editCustomer = (req, res) =>{

	//console.log()

	let updates = {
		fullName: req.body.fullName
	}

	Customer.findByIdAndUpdate(req.params.customerId, updates, {new: true})
	.then(updated => res.send(updated))
	.catch(error => res.send(error))

}

module.exports.deleteCustomer = (req, res) => {

	Customer.findByIdAndDelete(req.params.customerId, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.rent = async (req, res) => {

	

	let isUserUpdate = await Customer.findById(req.params.customerId)
	.then(user => {
		console.log(req.body.itemId)

		let rentItem = {
		title: req.body.title,
		itemId: req.body.itemId,
		price: req.body.price,
		type: req.body.type
	}

		user.rents.push(rentItem)
		return user.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
	})	
}

module.exports.viewRent = (req, res) => {

	Customer.findById(req.params.customerId)
	.then(result => res.send(result.rents))
}