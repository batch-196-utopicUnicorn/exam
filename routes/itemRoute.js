const express = require('express')
const router = express.Router()

const itemController = require('../controllers/itemControllers')
console.log('Connected to item controller')


router.post('/new', itemController.newItem)
router.post('/singleItem/', itemController.singleItem)
router.get('/', itemController.viewAllItems)
router.put('/edit/:itemId', itemController.editItem)
router.delete('/delete/:itemId', itemController.deleteItem)
//router.post('/rent/:customerId', itemController.rent)


module.exports = router;