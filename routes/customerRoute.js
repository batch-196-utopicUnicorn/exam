const express = require('express')
const router = express.Router()

const customerController = require('../controllers/customerControllers')
console.log('Connected to customer controller')


router.post('/new', customerController.newCustomer)
router.get('/', customerController.viewAllCustomer)
router.put('/edit/:customerId', customerController.editCustomer)
router.delete('/delete/:customerId', customerController.deleteCustomer)
router.post('/rent/:customerId', customerController.rent)
router.get('/viewRent/:customerId', customerController.viewRent)

module.exports = router;