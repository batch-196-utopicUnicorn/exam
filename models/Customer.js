const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
	fullName:{
		type: String,
		required: [true, "Complete name is required"]
	},
	rents: [
		{	
			title:{
				type: String,
				required: [true, "Title is required"]
			},
			itemId:{
				type: String,
				required: [true, "Item ID is required"]
			},
			type:{
				type: String,
				required: [true, "Video type is required"]
			},
			price:{
				type: Number,
				required: [true, "Price is required"]
			},

			timeRented:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Customer", customerSchema)