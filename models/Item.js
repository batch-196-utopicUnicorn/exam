const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({

	title:{
		type: String,
		required: [true, "Video title is required"]
	},
	price:{
		type: Number,
		required: [true, "Price for the video is required"]
	},
	type:{
		type: String,
		required: [true, "Video type required"]
	}
})

module.exports = mongoose.model("Item", itemSchema)